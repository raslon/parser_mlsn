import requests
import fake_useragent
from .proxy import *

class Connection():
    def __init__(self):
        ua = fake_useragent.UserAgent()
        self.headers = {'User-Agent': str(ua.random)}
        self.session = requests.Session()
        self.proxy = proxy_rotator()
    def get_html(self, link, cookies=None, header: dict = None):

        if header:
            self.headers.update(header)
        self.session.headers.update(self.headers)
        if self.proxy:
            self.session.proxies = {'http': 'http://{}'.format(self.proxy), 'https': 'https://{}'.format(self.proxy)}
        if cookies:
            return self.session.get(link, cookies=cookies, timeout=30).text  # ,
        else:
            return self.session.get(link, timeout=30).text

    def get_img(self, link, cookies=None, header: dict = None):

        if header:
            self.headers.update(header)
        self.session.headers.update(self.headers)
        if self.proxy:
            self.session.proxies = {'http': 'http://{}'.format(self.proxy), 'https': 'https://{}'.format(self.proxy)}
        if cookies:
            img = self.session.get(link, cookies=cookies, timeout=30)  # ,
            if img.status_code == 200:
                return img
            else:
                return None
        else:
            img = self.session.get(link, timeout=30)  # ,
            if img.status_code == 200:
                return img
            else:
                return None

    def __enter__(self):
        # print("__enter__")
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        # print("__exit__")
        self.session.close()
