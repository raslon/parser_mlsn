import json
import os
import re
import unicodedata
from io import StringIO, BytesIO
from functools import partial
from multiprocessing import Pool

from PIL import Image
from .connection import *
import bs4


class MLSN(Connection):
    bs = partial(bs4.BeautifulSoup, features='lxml')
    findingsre = re.compile(r'window\.\_\_INITIAL_STATE\_\_\ \=\ (.*)', re.MULTILINE)
    page = 0
    # params = ['Этаж',
    #           'Площадь общая',
    #           'Площадь жилая',
    #           'Площадь кухни',
    #           'Планировка',
    #           'Санузел',
    #           'Балкон',
    #           'Состояние/ремонт',
    #           'Покрытие пола',
    #           'Материал окон',
    #           'Входная дверь',
    #           'Вид из окон',
    #           'Угловая',
    #           'Домофон',
    #           'Счётчики воды',
    #           'Тип дома',
    #           'Материал дома',
    #           'Новостройка',
    #           'Год постройки',
    #           'Высота потолка',
    #           'Интернет',
    #           'Лифт',
    #           'Мусоропровод',
    #           'Парковка'
    #           ]
    # Материал
    material = {
        100: "не указано",
        23: "не указано",
        0: "не указано",
        1: "кирпичный",
        2: "панельный",
        3: "монолитный",
        5: "рубленый",
        6: "брусовой",
        7: "блочный",
        8: "шлакоблочный",
        10: "монолитно-кирпичный",
        11: "кирпично-насыпной",
        14: "каркасно-насыпной",
        125: "прочее",
    }
    # Тип дома
    buildingType = {
        100: "не указано",
        0: "не указано",
        1: "хрущевка",
        2: "сталинка",
        3: "ленинградская серия",
        4: "омская серия",
        5: "брежневка",
        6: "серия 121",
        7: "серия 97",
        8: "элитный",
        9: "полнометражный",
        10: "индивидуальный проект",
        11: "серия 464",
        12: "старый фонд",
        13: "улучшенный",
        14: "малосемейка",
    }
    # Планировка
    planning = {
        100: "не указано",
        0: "не указано",
        2: "изолированные комнаты",
        3: "смежные комнаты",
        4: "смежно-изолир. комнаты",
        5: "свободная планировка",
        1: "студия"
    }
    # Ремонт
    renovationType = {
        100: "не указано",
        0: "не указано",
        11: "удовлетворительное состояние",
        3: "хорошее состояние",
        10: "отличное состояние",
        2: "косметический ремонт",
        4: "евроремонт",
        5: "дизайнерский ремонт",
        1: "требует ремонта",
        6: "черновая отделка",
        7: "чистовая отделка",
        12: "ремонт от застройщика",
    }
    # Балкон
    balcony = {
        100: "не указано",
        0: "нет",
        1: "балкон",
        2: "лоджия",
        3: "балкон и лоджия",
        4: "2 балкона",
        5: "2 лоджии",
        6: "2 балкона и лоджия",
        7: "2 лоджии и балкон"
    }
    # Покрытие пола
    floorCovering = {
        100: "не указано",
        0: "не указано",
        1: "линолеум",
        2: "ламинат",
        3: "ковролин",
        4: "паркет",
        5: "половая доска",
        6: "без покрытия",
    }
    # Входная дверь
    outerDoorType = {
        100: "не указано",
        0: "не указано",
        1: "деревянная",
        2: "металлическая",
        3: "двойная метал/дерев",
        4: "двойная дерев/метал"
    }
    # Окна
    windowType = {
        100: "не указано",
        0: "не указано",
        1: "деревянные",
        2: "пластиковые (ПВХ)"
    }
    # Вид из окон
    windowView = {
        100: "не указано",
        0: "не указано",
        1: "во двор",
        2: "на улицу",
        3: "на улицу и во двор"
    }
    # Парковка
    parkingType = {
        100: "не указано",
        0: "не указано",
        1: "не указано",
        2: "отсутствует",
        3: "наземная парковка",
        4: "многоуровневая парковка",
        5: "подземная парковка"
    }
    # Санузел
    sanitary = {
        100: "не указано",
        1: "совмещенный",
        2: "раздельный",
        3: "2 с/у",
        4: "нет с/у"
    }
    # Отопление
    heating = {
        100: "не указано",
        1: "центральное",
        2: "газовое",
        3: "водяное",
        4: "электрическое",
        5: "печное"
    }

    def get_links(self, url):
        findingslinks = []
        page = 0
        with Connection() as connect:
            while True:
                page = page + 1
                print(page)
                html = connect.get_html(url.format(page))
                soup = self.bs(html)
                links = soup.find_all('a', {'class': 'link'})
                for link in links:
                    findingslinks.append(link['href'])
                    # print(link['href'])

                if soup.find('li', {'class': 'next disabled'}):
                    break
        return findingslinks

    def ifIntNone(self, info):
        if info is None:
            return 100
        else:
            return int(info)

    def isNone(self, info):
        if info:
            return True
        elif info == None:
            return None
        else:
            return False

    def get_data_apartments(self, link):
        # try:
        with Connection() as connect:
            data = {}
            images = []

            html = connect.get_html(link)
            soup = self.bs(html)

            raw_data = soup.find('script', text=self.findingsre)
            findings = json.loads(self.findingsre.search(str(raw_data)).group(1))
            # Ид
            data['id'] = findings['property']['data']['id']
            # URL
            data['link'] = link
            # Заголовок
            data['title'] = soup.find('h1', {'class': 'Title__base fonts__mainFont'}).text.strip()
            # Цена
            data['price'] = (findings['property']['data']['price'])
            # Продовец
            data['contactName'] = findings['property']['data']['contactName']
            # Номер телефона
            data['contactPhones'] = findings['property']['data']['contactPhones'][0]['number']
            # Регион
            data['region'] = findings['property']['data']['location']['region']['fullName']
            # Город
            data['city'] = findings['property']['data']['location']['locality']['fullName']
            # Район
            # data['localityDistrict'] = self.isNone(findings['property']['data']['location']['localityDistrict']['fullName'])
            # Улаца
            data['street'] = findings['property']['data']['location']['street']['fullName']
            # Дом
            data['house'] = findings['property']['data']['location']['house']['fullName']
            # # Микрорайон
            # data['microdistrict'] = findings['property']['data']['location']['microdistrict']['fullName']
            # Общая площадь
            data['squareTotal'] = findings['property']['data']['squareTotal']
            # Площадь кухни
            data['squareKitchen'] = findings['property']['data']['squareKitchen']
            # Площадь жилья
            data['squareLiving'] = findings['property']['data']['squareLiving']
            # Этаж
            data['floor'] = findings['property']['data']['floor']
            # Кол.этажей
            data['floorTotal'] = findings['property']['data']['floorTotal']
            # Материал дома
            data['material'] = self.material[self.ifIntNone(findings['property']['data']['materialId'])]
            # Тип дома
            data['buildingType'] = self.buildingType[self.ifIntNone(findings['property']['data']['buildingTypeId'])]
            # Планировка
            data['planning'] = self.planning[self.ifIntNone(findings['property']['data']['planningId'])]
            # Санузел
            data['sanitary'] = self.sanitary[self.ifIntNone(findings['property']['data']['sanitaryId'])]
            # Балкон
            data['balcony'] = self.balcony[self.ifIntNone(findings['property']['data']['balconyId'])]
            # Вид из окна
            data['windowView'] = self.windowView[self.ifIntNone(findings['property']['data']['windowViewId'])]
            # Покрытие пола
            data['floorCovering'] = self.floorCovering[self.ifIntNone(findings['property']['data']['floorCoveringId'])]
            # Ремонт
            data['renovationType'] = self.renovationType[
                self.ifIntNone(findings['property']['data']['renovationTypeId'])]
            # Входная Дверь
            data['outerDoorType'] = self.outerDoorType[self.ifIntNone(findings['property']['data']['outerDoorTypeId'])]
            # Материал окон
            data['windowType'] = self.windowType[self.ifIntNone(findings['property']['data']['windowTypeId'])]
            # Парковка
            data['parkingType'] = self.parkingType[self.ifIntNone(findings['property']['data']['parkingTypeId'])]
            # Высота потолка
            data['ceilingHeight'] = findings['property']['data']['ceilingHeight']
            # Интернет
            data['ceilingHeight'] = self.isNone(findings['property']['data']['hasInternet'])
            # Домофон
            data['hasIntercom'] = self.isNone(findings['property']['data']['hasIntercom'])
            # Счетчик воды
            data['hasAquacount'] = self.isNone(findings['property']['data']['hasAquacount'])
            # Лифт
            data['hasElevator'] = self.isNone(findings['property']['data']['hasElevator'])
            # Мусоропровод
            data['hasRubbishChute'] = self.isNone(findings['property']['data']['hasRubbishChute'])
            # Новостройка
            data['isNewBuilding'] = self.isNone(findings['property']['data']['isNewBuilding'])
            # Угловая
            data['isCorner'] = self.isNone(findings['property']['data']['isCorner'])
            # Год постройки
            data['builtYear'] = findings['property']['data']['builtYear']
            # Описание
            data['description'] = findings['property']['data']['description']

            # Фотки
            for photo in findings['property']['data']['photos']['all']:
                images.append(photo['url'].replace('{{ size }}', 'large'))
            if not os.path.isdir('images/{}'.format(str(data['id']))):
                os.mkdir('images/{}'.format(str(data['id'])))
            for i, image in enumerate(images):
                img = connect.get_img(image)
                if img:
                    im = Image.open(BytesIO(img.content))
                    rgb_im = im.convert('RGB')
                    rgb_im.save('images/{}/{}.jpg'.format(data['id'], i))

            return data
        # except Exception as e:
        #     print('Error', link, e)

        # title = soup.find('h1', {'class': 'Title__base fonts__mainFont'})
        # features = soup.find_all('tr')
        # for featur in features:
        #     key = featur.find('td', {'class': 'ParamsList__paramName typography__bodySmall fonts__mainFont'})
        #     value = featur.find('td', {'class': 'ParamsList__paramValue typography__body fonts__mainFont'})
        #     if key:
        #         if key.text.strip() in self.params:
        #             print(key.text.strip(), value.text.strip())
        #
        # images = soup.find_all('img', {'class': 'Image__loading'})

    def get_data_lands(self, link):
        # try:
        with Connection() as connect:
            data = {}
            images = []

            html = connect.get_html(link)
            soup = self.bs(html)

            raw_data = soup.find('script', text=self.findingsre)
            findings = json.loads(self.findingsre.search(str(raw_data)).group(1))
            # Ид
            data['id'] = findings['property']['data']['id']
            # URL
            data['link'] = link
            # Заголовок
            data['title'] = unicodedata.normalize("NFKD", soup.find('h1', {
                'class': 'Title__base fonts__mainFont'}).text.strip())
            # Цена
            data['price'] = (findings['property']['data']['price'])
            # Продовец
            data['contactName'] = findings['property']['data']['contactName']
            # Номер телефона
            data['contactPhones'] = findings['property']['data']['contactPhones'][0]['number']
            # Регион
            # data['region'] = findings['property']['data']['location']['region']['fullName']
            # Город
            # data['city'] = findings['property']['data']['location']['locality']['fullName']
            # Общая площадь
            data['squareTotal'] = findings['property']['data']['squareTotal']
            # Площадь участка
            data['squareLand'] = findings['property']['data']['squareLand']
            # Площадь кухни
            data['squareKitchen'] = findings['property']['data']['squareKitchen']
            # Площадь жилья
            data['squareLiving'] = findings['property']['data']['squareLiving']
            # # Этаж
            # data['floor'] = findings['property']['data']['floor']
            # Кол.этажей
            data['floorTotal'] = findings['property']['data']['floorTotal']
            # Вид отопления
            data['heating'] = self.heating[self.ifIntNone(findings['property']['data']['heatingId'])]

            # Материал дома
            data['material'] = self.material[self.ifIntNone(findings['property']['data']['materialId'])]
            # Ремонт
            data['renovationType'] = self.renovationType[
                self.ifIntNone(findings['property']['data']['renovationTypeId'])]
            # Электричество
            data['hasElectricity'] = 'есть' if findings['property']['data']['hasElectricity'] else 'нет'
            # Водопровод
            data['hasWater'] = 'есть' if findings['property']['data']['hasWater'] else 'нет'
            # Газ
            data['hasGas'] = 'есть' if findings['property']['data']['hasGas'] else 'нет'
            # Канализация
            data['hasCanalization'] = 'есть' if findings['property']['data']['hasCanalization'] else 'нет'
            # Охрана
            data['hasSecurityProtection'] = 'есть' if findings['property']['data']['hasSecurityProtection'] else 'нет'
            # Гараж
            data['hasGarage'] = 'есть' if findings['property']['data']['hasGarage'] else 'нет'
            # Баня
            data['hasBathhouse'] = 'есть' if findings['property']['data']['hasBathhouse'] else 'нет'
            # Хоз.постройки
            data['hasOutbuildings'] = 'есть' if findings['property']['data']['hasOutbuildings'] else 'нет'
            # Описание
            data['description'] = unicodedata.normalize("NFKD",
                                                        findings['property']['data']['description'].replace('\n', ''))

            # Фотки
            for photo in findings['property']['data']['photos']['all']:
                images.append(photo['url'].replace('{{ size }}', 'large'))
            if not os.path.isdir('images/{}'.format(str(data['id']))):
                os.mkdir('images/{}'.format(str(data['id'])))
            for i, image in enumerate(images):
                img = connect.get_img(image)
                if img:
                    im = Image.open(BytesIO(img.content))
                    rgb_im = im.convert('RGB')
                    rgb_im.save('images/{}/{}.jpg'.format(data['id'], i))

            return data
        # except Exception as e:
        #     print('Error', link, e)

        # title = soup.find('h1', {'class': 'Title__base fonts__mainFont'})
        # features = soup.find_all('tr')
        # for featur in features:
        #     key = featur.find('td', {'class': 'ParamsList__paramName typography__bodySmall fonts__mainFont'})
        #     value = featur.find('td', {'class': 'ParamsList__paramValue typography__body fonts__mainFont'})
        #     if key:
        #         if key.text.strip() in self.params:
        #             print(key.text.strip(), value.text.strip())
        #
        # images = soup.find_all('img', {'class': 'Image__loading'})

    def get_data_commercials(self, link):
        # try:
        with Connection() as connect:
            data = {}
            images = []

            html = connect.get_html(link)
            soup = self.bs(html)

            raw_data = soup.find('script', text=self.findingsre)
            findings = json.loads(self.findingsre.search(str(raw_data)).group(1))
            # Ид
            data['id'] = findings['property']['data']['id']
            # URL
            data['link'] = link
            # Заголовок
            data['title'] = unicodedata.normalize("NFKD", soup.find('h1', {
                'class': 'Title__base fonts__mainFont'}).text.strip())
            # Цена
            data['price'] = (findings['property']['data']['price'])
            # Продовец
            data['contactName'] = findings['property']['data']['contactName']
            # Номер телефона
            data['contactPhones'] = findings['property']['data']['contactPhones'][0]['number']
            # Регион
            # data['region'] = findings['property']['data']['location']['region']['fullName']
            # Город
            # data['city'] = findings['property']['data']['location']['locality']['fullName']
            # Общая площадь
            data['squareTotal'] = findings['property']['data']['squareTotal']
            # Площадь участка
            data['squareLand'] = findings['property']['data']['squareLand']

            # # Этаж
            data['floor'] = findings['property']['data']['floor']
            # Кол.этажей
            data['floorTotal'] = findings['property']['data']['floorTotal']

            # # Материал дома
            # data['material'] = self.material[self.ifIntNone(findings['property']['data']['materialId'])]
            # # Ремонт
            # data['renovationType'] = self.renovationType[
            #     self.ifIntNone(findings['property']['data']['renovationTypeId'])]
            # # Электричество
            # data['hasElectricity'] = 'есть' if findings['property']['data']['hasElectricity'] else 'нет'
            # # Водопровод
            # data['hasWater'] = 'есть' if findings['property']['data']['hasWater'] else 'нет'
            # # Газ
            # data['hasGas'] = 'есть' if findings['property']['data']['hasGas'] else 'нет'
            # # Канализация
            # data['hasCanalization'] = 'есть' if findings['property']['data']['hasCanalization'] else 'нет'
            # # Охрана
            # data['hasSecurityProtection'] = 'есть' if findings['property']['data']['hasSecurityProtection'] else 'нет'
            # # Гараж
            # data['hasGarage'] = 'есть' if findings['property']['data']['hasGarage'] else 'нет'
            # # Баня
            # data['hasBathhouse'] = 'есть' if findings['property']['data']['hasBathhouse'] else 'нет'
            # # Хоз.постройки
            # Первая линия
            data['isFirstLine'] = 'да' if findings['property']['data']['isFirstLine'] else 'нет'
            # Отдельностоящее здание
            data['detachedBuilding'] = 'да' if findings['property']['data']['detachedBuilding'] else 'нет'
            # Отдельный вход
            data['ownEntrance'] = 'да' if findings['property']['data']['ownEntrance'] else 'нет'
            # Описание
            data['description'] = unicodedata.normalize("NFKD",
                                                        findings['property']['data']['description'].replace('\n', ''))

            # Фотки
            for photo in findings['property']['data']['photos']['all']:
                images.append(photo['url'].replace('{{ size }}', 'large'))
            if not os.path.isdir('images/{}'.format(str(data['id']))):
                os.mkdir('images/{}'.format(str(data['id'])))
            pic = []
            for i, image in enumerate(images):
                img = connect.get_img(image)

                if img:
                    im = Image.open(BytesIO(img.content))
                    rgb_im = im.convert('RGB')
                    path = 'images/{}/{}.jpg'.format(data['id'], i)
                    pic.append(path)
                    rgb_im.save(path)
            data['images'] = pic
            return data
        # except Exception as e:
        #     print('Error', link, e)

        # title = soup.find('h1', {'class': 'Title__base fonts__mainFont'})
        # features = soup.find_all('tr')
        # for featur in features:
        #     key = featur.find('td', {'class': 'ParamsList__paramName typography__bodySmall fonts__mainFont'})
        #     value = featur.find('td', {'class': 'ParamsList__paramValue typography__body fonts__mainFont'})
        #     if key:
        #         if key.text.strip() in self.params:
        #             print(key.text.strip(), value.text.strip())
        #
        # images = soup.find_all('img', {'class': 'Image__loading'})

    def get_data_garages(self, link):
        # try:
        with Connection() as connect:
            data = {}
            images = []

            html = connect.get_html(link)
            soup = self.bs(html)

            # Ид
            try:
                data['id'] = soup.find('span', {'id': 'card_id'}).text.strip()
            except:
                print(link)
            # URL
            data['link'] = link
            # Заголовок
            data['title'] = unicodedata.normalize("NFKD", soup.find('h1', {
                'class': 'header'}).text.strip())

            discriptions = soup.find_all('div', {'class': 'description'})
            for discription in discriptions:
                for image in discription.find_all('img'):
                    images.append(image['src'].replace('medium', 'large'))
                if discription.find('p'):
                    data['discription'] = discription.find('p').text.strip()

            raw_params = soup.find('div', {'class': 'row-fluid params'})
            # print(raw_params)
            params = raw_params.find_all('tr')

            for param in params:
                key = param.find('th').text.strip()
                val = param.find('td').text.strip()
                data[key] = val

            # Фотки
            if not os.path.isdir('images/{}'.format(str(data['id']))):
                os.mkdir('images/{}'.format(str(data['id'])))
            pic = []
            for i, image in enumerate(images):
                img = connect.get_img(image)

                if img:
                    im = Image.open(BytesIO(img.content))
                    rgb_im = im.convert('RGB')
                    path = 'images/{}/{}.jpg'.format(data['id'], i)
                    pic.append(path)
                    rgb_im.save(path)
            data['images'] = pic
            return data
        # except Exception as e:
        #     print('Error', link, e)

        # title = soup.find('h1', {'class': 'Title__base fonts__mainFont'})
        # features = soup.find_all('tr')
        # for featur in features:
        #     key = featur.find('td', {'class': 'ParamsList__paramName typography__bodySmall fonts__mainFont'})
        #     value = featur.find('td', {'class': 'ParamsList__paramValue typography__body fonts__mainFont'})
        #     if key:
        #         if key.text.strip() in self.params:
        #             print(key.text.strip(), value.text.strip())
        #
        # images = soup.find_all('img', {'class': 'Image__loading'})

    def get_sale_Demand(self):
        dataes = []
        while True:
            with Connection() as connect:
                self.page = self.page+1
                print(self.page)
                data = {}
                images = []
                html = connect.get_html('https://omsk.mlsn.ru/office/saleDemand/index/?SaleDemand_page={}&minprice=&maxprice=&user_type=3&added=&yt0=&pageSize=100'.format(self.page))
                soup = self.bs(html)

                table = soup.find('table', {'class': 'items table table-bordered'})
                tbody = table.find('tbody')
                tr = tbody.find_all('tr')

                for tr_ in tr:
                    td = tr_.find_all('td')
                    data['type'] = td[0].text.strip()
                    data['district'] = [x.text.strip() for x in td[1].find_all('div')]
                    data['max_price'] = td[2].text.strip()
                    data['contact'] = td[3].text.strip()
                    data['comment'] = td[4].text.strip()
                    data['date'] = td[5].text.strip()
                    dataes.append(data)
                pagination = soup.find('div', {'class':'pagination'})
                if pagination:
                    li = pagination.find_all('li', {'class': 'disabled'})
                    if li[-1].text.strip() =='»':
                        self.page = 0
                        break
                else:
                    self.page = 0
                    break