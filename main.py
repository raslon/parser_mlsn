import json
import os
from multiprocessing.pool import Pool

from parsers.mlsn import MLSN

def run():
    mlsn = MLSN()
    sources = {
        'apartments': {'method': mlsn.get_data_apartments, 'url': 'https://omsk.mlsn.ru/pokupka-nedvizhimost/?onlyOwners=1&page={}'},
        'apartmentsinNew': {'method': mlsn.get_data_apartments, 'url':'https://omsk.mlsn.ru/pokupka-nedvizhimost/?isNewBuilding=1&onlyOwners=1&page={}'},
        'lands': {'method': mlsn.get_data_lands, 'url':'https://omsk.mlsn.ru/pokupka-dachi-i-uchastki/?hasPhoto=1&onlyOwners=1&page={}'},
        'commercials': {'method': mlsn.get_data_commercials, 'url':'https://omsk.mlsn.ru/pokupka-kommercheskaja-nedvizhimost/?hasPhoto=1&onlyOwners=1&page={}'},
        'garages': {'method': mlsn.get_data_garages, 'url':'https://omsk.mlsn.ru/pokupka-garazhi/?hasPhoto=1&onlyOwners=1&page={}'}
    }

    for source in sources:

        url = sources[source]['url']
        links = mlsn.get_links(url)
        if not os.path.isdir('images'):
            os.mkdir('images')
        print(links)
        with open('{}.json'.format(source), 'w', encoding='utf-8') as f:
            with Pool(10) as pool:
                p = pool.map(sources[source]['method'], links)
                print(len(p))
                print(p)
                json.dump(p, f, ensure_ascii=False)

    print(mlsn.get_sale_Demand())


if __name__ == '__main__':
    run()